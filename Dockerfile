# https://neurotolge.ee/
# .. või tõmmake endale alla meie tõlkemudel ja skriptid GitHub’i lehelt.

FROM ubuntu:16.04

MAINTAINER No Maintenance Intended
LABEL description="nazgul :: NMT provider implementation by TartuNLP"

RUN apt-get update && apt-get install -y curl

#RUN curl -s -J -O 'https://owncloud.ut.ee/owncloud/index.php/s/sq9FebWmBNe9JGZ/download?path=%2F&files=en-et-lv.tgz'
#RUN curl -s -J -O 'https://owncloud.ut.ee/owncloud/index.php/s/sq9FebWmBNe9JGZ/download?path=%2F&files=en-et-lv-ru.tgz'
#RUN curl -s -J -O 'https://owncloud.ut.ee/owncloud/index.php/s/sq9FebWmBNe9JGZ/download?path=%2F&files=en-fr-de.tgz'
#RUN curl -s -J -O 'https://owncloud.ut.ee/owncloud/index.php/s/sq9FebWmBNe9JGZ/download?path=%2F&files=septilang.tgz'


RUN apt-get install -y \
            build-essential \
            git-core \
            pkg-config \
            libtool \
            zlib1g-dev \
            libbz2-dev \
            cmake \
            automake \
            python-dev \
            perl \
            libsparsehash-dev \
            libboost-all-dev \
            curl \
            python-pip \
        && rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/barvins/amunmt.git
RUN cd amunmt && mkdir -p build && cd build && cmake .. -DCUDA:bool=OFF && make -j
RUN amunmt/build/bin/amun --help > /dev/null
RUN cp /amunmt/build/src/libamunmt.so /usr/lib/python2.7/

RUN pip install --upgrade pip && pip install nltk
RUN python -c "import nltk; nltk.download('perluniprops'); nltk.download('punkt'); nltk.download('nonbreaking_prefixes')"

RUN git clone https://github.com/TartuNLP/nazgul.git
RUN echo "wait for nazgul to be fixed - no moses in nltk"
